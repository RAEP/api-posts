<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PostCommentsRealtionshipCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request){

        $post = $this->additional["post"];

        return [
            'data' => CommentIndetifierResource::collection($this->collection),
            'links' => [

                'self' => route('posts.relationship.comments',  $post),
                'related' => route('posts.comments',  $post)
            ]
        ];
    }
}

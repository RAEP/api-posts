<?php

use App\Models\Comment;
use App\Models\Post;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => 'Duilio Palacios',
            'email' => 'duilio@styde.net',
            'password' => bcrypt('laravel'),
            'admin' => true,
        ]);
        factory(User::class)->times(10)->create();
        factory(Post::class)->times(5)->create();
        factory(Comment::class)->times(3)->create();


        //$this->call(UserSeeder::class);
    }
}
